import javax.swing.JOptionPane;

/**
 * Purpose:
 * Demonstrate how to +,-,/,*,%(modulation) returns a remainder
 * 3/2 = 1 r 1
 * 10/3 = 3 r 1
 * 11/3 = 3 r 2
 * Date:9/27/17
 * @author Nadifo
 *
 */
public class Arithmetic {

	public static void main(String[] args) {
		String strLaborHours = "";
		String strHourRates = "";
		String strTaxRates = "";
		double dblLaborHours, dblHourRates, dblTaxRates, dblPayAmount, dblTaxAmount, dblNetPay;
		
		//prompt the user by using JOptionPane input dialog
		strLaborHours = JOptionPane.showInputDialog("Enter the labor hours ");
		strHourRates = JOptionPane.showInputDialog("Enter the hour rates ");
		strTaxRates = JOptionPane.showInputDialog("Enter the tax rates");
		
		//a wrapper class is a class of the primitive data types.
		dblLaborHours = Double.parseDouble(strLaborHours);
		dblHourRates = Double.parseDouble(strHourRates);
		dblTaxRates = Double.parseDouble(strTaxRates);
		dblPayAmount = dblLaborHours * dblHourRates;
		dblTaxAmount = dblTaxRates * dblPayAmount;
		dblNetPay = dblPayAmount - dblTaxAmount;
		
		JOptionPane.showMessageDialog(null, "The labor hour(s) is " + strLaborHours + ". \n and the hour rates is $" + strHourRates + " / hour. \n " + "The pay amount is $" + dblPayAmount + ".\n Tax amount is $" + dblTaxAmount + ".\n The net payment is $" + dblNetPay + "." );

		
	}

}
